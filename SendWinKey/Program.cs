﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace SendWinKey
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            if (args.Length == 0 || args[0].Length == 0)
            {
                SendWin(null);
            }
            else if(args.Contains("--help") || args.Contains("/?") )
            {
                ShowUsage();
            }
            else
            {
                SendWin(args[0][0]);
            }
            
        }

        /// <summary>
        /// 使い方を出力します。
        /// </summary>
        static void ShowUsage()
        {
            MessageBox.Show(@"Winキーと任意のキーを同時に押すキーストロークをシミュレートします。

SendWinKey.exe [Key]

Key: Winキーと同時に押すキーを1文字指定します。
     1文字以上指定した場合は、最初の文字のみが使用されます。
     このオプションを省略した場合は、Winキーだけが送信されます。

（例）Win+Cを送信する: SendWinKey.exe C");

            
        }

        /// <summary>
        /// Winキー+任意のキーを送信します
        /// </summary>
        /// <param name="key"></param>
        static void SendWin(char? key)
        {
            SendKey(Win32.VK_LWIN, true);

            if (key.HasValue)
            {
                ushort code = (ushort)char.ToUpper(key.Value);
                SendKey(code, true);
                SendKey(code, false);
            }

            SendKey(Win32.VK_LWIN, false);

            
        }

        /// <summary>
        ///  キーを送信します
        /// </summary>
        /// <param name="key">キーコード</param>
        /// <param name="keyup">true: キーダウンを送信する false: キーアップを送信する </param>
        static void SendKey(ushort key, bool keydown)
        {

            Win32.INPUT input = new Win32.INPUT();
            input.type = Win32.INPUT_KEYBOARD;
            input.ki.wScan = 0;
            input.ki.time = 0;
            input.ki.dwExtraInfo = Win32.GetMessageExtraInfo();

            input.ki.wVk = key;
            input.ki.dwFlags = keydown ? (uint)0 : Win32.KEYEVENTF_KEYUP;

            Win32.SendInput(1, ref input, Marshal.SizeOf(typeof(Win32.INPUT)));
        }
    }
}
